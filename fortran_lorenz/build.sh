#!/bin/bash
set -o nounset
set -o errexit

FC=mpif90
FCOPTS='-fimplicit-none -Wall -fbounds-check'

$FC -o l63.exec $FCOPTS Lorenz63.f90
$FC -o l63_empire.exec $FCOPTS Lorenz63_empire.f90

echo 'Compilation succesful'
exit 0
