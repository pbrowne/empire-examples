#!/bin/bash
set -o nounset
set -o errexit

build.sh
echo 'Removing executables'
rm l63.exec l63_empire.exec
exit 0
