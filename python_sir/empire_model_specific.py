#! /usr/bin/env python
# Time-stamp: <2014-07-02 16:38:02 pbrowne>
# Python original by Philip Browne

###############################################################################
#    empire_model_specific.py contains the routines that need to be changed
#    for each different model and observation network
#
#The MIT License (MIT)
#
#Copyright (c) 2014 Philip A. Browne
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#
#Email: p.browne@reading.ac.uk
###############################################################################


#  this is set up for Lorenz63 system
#
#       ( 0 )
#   H = ( 0 )
#       ( 1 )
#
#
#   R = ( 2^0.5 )
#
#
#                  (  1   0.5  0.25 )
#   Q = 2*(0.01)^2*( 0.5   1    0.5 )
#                  (0.25  0.5    1  )




import numpy as np
def configure_model():
    state_dim = 3
    num_iters = 100

    return state_dim,num_iters

def H(x):
    H = np.array([0,0,1])
    m,n = np.shape(x)
    y = np.zeros((m,1), dtype='float64')
    for i in range(m):
        y[i,:] = H.dot(x[i,:])
    #y = np.reshape(y,(1,n))
    return y

def Qhalf(x):
    Qhalf = np.array([[1.33333333,-0.66666667,0.],[-0.66666667,1.66666667,-0.66666667],[0.,-0.66666667,1.33333333]])
    m,n = np.shape(x)
    v = np.zeros_like(x)
    for i in range(m):
        v[i,:] = Qhalf.dot(x[i,:])
    #v = Qhalf.dot(x)
    v = v*np.sqrt(2)*0.01
    return v

def solve_r(y):
    v = y/np.sqrt(2)
    return v
