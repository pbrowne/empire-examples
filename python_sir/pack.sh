#!/bin/bash
set -o nounset
set -o errexit



tarname='python_empire_sir.tar'

rm -f $tarname

tar -cvf $tarname empire_model_specific.py empire_obs.py empire_random.py empire_resample.py empire_sir.py LICENSE.txt




