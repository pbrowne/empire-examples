#! /usr/bin/env python
# Time-stamp: <2014-11-12 10:36:25 pbrowne>
# Python original by Philip Browne

###############################################################################
#    empire_sir.py Implements the SIR filter with EMPIRE coupling
#
#The MIT License (MIT)
#
#Copyright (c) 2014 Philip A. Browne
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#
#Email: p.browne@reading.ac.uk
###############################################################################

from mpi4py import MPI #import MPI stuff
from empire_model_specific import *
from empire_random import *
from empire_obs import *
from empire_resample import resample_uir
import sys
import numpy as np

###############################################################################

class emp:
    nens=None
    nda=None
    pfrank=None
    mype_id=None
    particles=None
    count=None


def initialise_mpi():

    da_colour = 10000
    couple_colour = 9999

    mpi_comm_world = MPI.COMM_WORLD
    da = 1
    world_id = mpi_comm_world.Get_rank()
    world_size=mpi_comm_world.Get_size()
    pf_mpi_comm = mpi_comm_world.Split(da,world_id)
    pfrank = pf_mpi_comm.Get_rank()
    nda = pf_mpi_comm.Get_size()
    cpl_mpi_comm = mpi_comm_world.Split(couple_colour,world_size)
    myrank=cpl_mpi_comm.Get_rank()
    nens = cpl_mpi_comm.Get_size()
    nens = nens - nda

    #lets find the particles:
    count = 0
    for particle in range(nens):
        if np.float64(particle) >= np.float64(nens*pfrank)/np.float64(nda) and np.float64(particle) < np.float64(nens*(pfrank+1))/np.float64(nda):
            count = count + 1
    
    particles = np.zeros(count, dtype='i')
    rtmp = -1
    for particle in range(nens):
        if np.float64(particle) >= np.float64(nens*pfrank)/np.float64(nda) and np.float64(particle) < np.float64(nens*(pfrank+1))/np.float64(nda):
            rtmp = rtmp + 1
            particles[rtmp] = particle
    
    print 'PFrank = '+str(pfrank)+' and I own particles '+str(particles)
    emp.count = count
    emp.particles = particles
    emp.nens = nens
    emp.nda = nda
    emp.pfrank = pfrank
    emp.mype_id = world_id
    return emp,cpl_mpi_comm,pf_mpi_comm
#################################################################################


state_dim,num_iters = configure_model()

#state_vector = np.zeros(state_dim, dtype='float64')

#call subroutine to initialise EMPIRE
emp,cpl_mpi_comm,pf_mpi_comm = initialise_mpi() 

weight = np.zeros(emp.nens, dtype='float64')
x = np.zeros((emp.count,state_dim), dtype='float64')

#This is the initial receive from the model                                        
for i in range(emp.count):
    particle = emp.particles[i]
    cpl_mpi_comm.Recv([x[i,:],len(x[i,:]),MPI.DOUBLE],source=particle,tag=1)

print 'DA '+str(emp.pfrank)+' has received all initial state_vectors over mpi'

#Create an initial ensemble
normaln = normalrandomnumbers2d(emp.count,state_dim)
betan = Qhalf(normaln)
x = x + 10.0*betan

#start the timestepping loop
for iter in range(num_iters):
    for i in range(emp.count):
        particle = emp.particles[i]
        tag = 1
        cpl_mpi_comm.Send([x[i,:],len(x[i,:]),MPI.DOUBLE],dest=particle,tag=1)
        #print 'Particle filter '+str(emp.pfrank)+' has sent state_vector over mpi at iteration '+str(iter)+' to ensemble member '+str(particle)
        
    for i in range(emp.count):
        particle = emp.particles[i]
        tag = 1
        cpl_mpi_comm.Recv([x[i,:],len(x[i,:]),MPI.DOUBLE],source=particle,tag=1)
        #print 'receive just happened'
    #print 'all models received by particle filter at iteration '+str(iter)

    normaln = normalrandomnumbers2d(emp.count,state_dim)
    betan = Qhalf(normaln)

    x = x + betan 

    observation_time = True
    if(observation_time):
        y = get_observation_data(iter)
        Hx = H(x)
        #print np.shape(Hx)
        #print Hx
        #print Hx[0,:]
        #print Hx[1,:]
        #print Hx[2,:]
        for i in range(emp.count):
            particle = emp.particles[i]
            ytemp = solve_r(y-Hx[i,:])
            w = sum(ytemp*(y-Hx[i,:]))
            weight[particle] = 0.5*w

        resample_uir(emp,pf_mpi_comm,weight,x)


    
#now do the final send to the model
tag = 1
for i in range(emp.count):
    particle = emp.particles[i]
    tag = 1
    cpl_mpi_comm.Send([x[i,:],len(x[i,:]),MPI.DOUBLE],dest=particle,tag=1)
    print 'Particle filter '+str(emp.pfrank)+' has sent final state_vector over mpi to ensemble member '+str(particle)
