#! /usr/bin/env python
# Time-stamp: <2014-07-02 16:52:41 pbrowne>
# Python original by Philip Browne

###############################################################################
#    empire_resample.py implements Universal Importance Resampling
#    within the EMPIRE framework using Python
#
#The MIT License (MIT)
#
#Copyright (c) 2014 Philip A. Browne
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#
#Email: p.browne@reading.ac.uk
###############################################################################

#this isn't the nicest implementation, as mpi4py does not have all the 
#features of MPI, at least not documented anyway.
#these would be mpi_gatherv and mpi_i{send,recv}.
#So this may not be the most efficient thing in the world compared to
#my fortran implementation, but should serve to show what you have to do
#to send the state from place to place.
#
#the basic idea is that processor 0 makes one draw and computes an array BRANDNEW
#based on that. On completion, BRANDNEW[i], contains the number of the particle
#which should overwrite particle [i].
#
#It is pretty technical, so have a good go a understanding it, but please
#email me any remaining questions. however putting in lots of print statements
#will really help to understand the computations!
#
#In the words of Liam Neeson, "good luck".
#



import sys
import numpy as np
import random
from mpi4py import MPI
def resample_uir(emp,pf_mpi_comm,weight,x):

    location = np.zeros(emp.nens, dtype='int')
    da = 0
    for particle in range(emp.nens):
        if(np.float64(particle) < np.float64(emp.nens*(da+1))/np.float64(emp.nda)):
            location[particle] = da
        else:
            da = da + 1
            location[particle] = da
           
    for particle in range(emp.nens):
        weight[particle] = pf_mpi_comm.bcast(weight[particle], root=location[particle])

       




    #we have to just draw one random number, so this task is best
    #performed solely by the master processor...

    #print*,'ahoy ',pf%weight
    #generate the array BRANDNEW = [1,2,3,...,N]
    brandnew = range(emp.nens)

    if(emp.pfrank == 0 ):
        #check for NaN in the weight
        for i in range(emp.nens):
            if(weight[i] == np.NaN):
                print 'ERROR particle '+str(i)+' has weight NaN'
                sys.exit()
            

        #normalise the weights and store them as the actual values.
        #print*,'seriously: ',pf%weight
        #print weight
        weight = weight - np.mean(weight)
        #print weight
        weight = np.exp(weight)# + np.amin(weight) )
        weight = weight/sum(weight)
        #print weight
        #compute the cumulative weights, so cumweights(j) = sum_i=1^j weights(i)
        cumweights = np.zeros(len(weight), dtype='float64')
        for i in range(emp.nens):
            if(i == 0):
                cumweights[0] = weight[0]
            else:
                cumweights[i] = cumweights[i-1] + weight[i]


        #print cumweights
        #now make the draw of the single random variable
        #distributed normally between 0 and 1/(number of ensemble members)
        draw = random.random()
        #print draw
        draw = draw/np.float64(emp.nens)
        #print draw

        new = np.zeros(emp.nens, dtype='int16')
        old = 0

        #for each particle, compute the POINT given by the 
        #random draw.
        for particle in range(emp.nens):
            point = np.float64(particle)/np.float64(emp.nens) + draw
            #print point
            #Step through the weights of each of the particles
            #and find the 'bin' which it lies in.
            #store this in the array NEW.
            loop = True
            while loop:
                if(point < cumweights[old]):
                    new[particle] = old
                    break
                else:
                    old = old + 1
                

        #now we need to determine which particles have to be replaced:
        #initialise this array to something stupid:
        tobereplaced = np.zeros(emp.nens, dtype='int16')
        tobereplaced[:] = -1

        #loop over each particle
        for i in range(emp.nens):
            inn = False
            #go through the array NEW and if it does not exist 
            #in that array, then mark the array TOBEREPLACED
            #with the index of said particle
            for j in range(emp.nens):
                if(i == new[j]):
                    inn = True
                    break
                
            if( not inn ):
                tobereplaced[i] = i


        #dupepos is a counter storing the position of duplicated
        dupepos = 0

        #loop through all the particles
        for i in range(emp.nens):
            if(tobereplaced[i] != -1):
                #if the particle is to be replaced then
                #find the particle which has been duplicated
                for k in range(dupepos,emp.nens):
                    if(new[k] == new[k+1]):
                        dupe = new[k+1]
                        dupepos = k+1
                        break
                
                #place this duplicated particle into the place
                #in the array BRANDNEW that it corresponds to
                brandnew[tobereplaced[i]] = dupe
            
            
        print '##################################'
        print brandnew
        print '##################################'
    #end of master processor loop    
    

    #now the master processor can send the array
    #BRANDNEW to all the other processors
    #call mpi_bcast(brandnew,pf%nens,mpi_integer,0,pf_mpi_comm,mpi_err)
    brandnew=pf_mpi_comm.bcast(brandnew,0)#emp.nens,MPI.INTEGER,0)
    #print*,brandnew

    #check to see if any of this processors particles are being
    #replaced. if so, call the mpi receive
    
    for particle in range(emp.nens):
        if(brandnew[particle] != particle):
            #this has to be sent and received then
            #send from brandnew[particle]
            #recv at particle
            
            if(location[brandnew[particle]] == emp.pfrank and location[particle] == emp.pfrank):
                #just copy directly
                for i in range(emp.count):
                    if(emp.particles[i] == brandnew[particle]):
                        sendloc=i
                for i in range(emp.count):
                    if(emp.particles[i] == particle):
                        recvloc=i
                #print 'direct copy from '+str(sendloc)+' to '+str(recvloc)
                x[recvloc,:] = x[sendloc,:]
            elif(location[brandnew[particle]] == emp.pfrank):
                for i in range(emp.count):
                    if(emp.particles[i] == brandnew[particle]):
                        sendloc=i
                #print 'one sided send to '+str(location[particle])
                pf_mpi_comm.Send([x[sendloc,:],len(x[sendloc,:]),MPI.DOUBLE],dest=location[particle],tag=1)
            elif(location[particle] == emp.pfrank):
                for i in range(emp.count):
                    if(emp.particles[i] == particle):
                        recvloc=i
                #print 'one sided recv from '+str(location[brandnew[particle]])
                pf_mpi_comm.Recv([x[recvloc,:],len(x[recvloc,:]),MPI.DOUBLE],source=location[brandnew[particle]],tag=1)
                
            
    


    pf_mpi_comm.Barrier()

    #we have just resampled, so by construction the particles
    #have equal weights.
    weight = 1.0/np.float64(emp.nens)
    weight = -np.log(weight)
