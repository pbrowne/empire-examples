#! /usr/bin/env python
# Time-stamp: <2014-11-11 17:41:44 pbrowne>
# Python original by Philip Browne

###############################################################################
#    lorenz63_empire.py Implements Lorenz 1963 model with EMPIRE coupling
#
#The MIT License (MIT)
#
#Copyright (c) 2014 Philip A. Browne
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#
#Email: p.browne@reading.ac.uk
###############################################################################

from mpi4py import MPI #import MPI stuff
import sys
import numpy as np

def initialise_mpi():

    da_colour = 10000
    couple_colour = 9999

    mpi_comm_world = MPI.COMM_WORLD
    da = 1
    world_id = mpi_comm_world.Get_rank()
    world_size=mpi_comm_world.Get_size()
    pf_mpi_comm = mpi_comm_world.Split(da,world_id)
    pfrank = pf_mpi_comm.Get_rank()
    nda = pf_mpi_comm.Get_size()
    cpl_mpi_comm = mpi_comm_world.Split(couple_colour,world_size)
    myrank=cpl_mpi_comm.Get_rank()
    nens = cpl_mpi_comm.Get_size()
    nens = nens - nda

    #lets find the particles:
    count = 0
    for particle in range(nens):
        if np.float64(particle) >= np.float64(nens*pfrank)/np.float64(nda) and np.float64(particle) < np.float64(nens*(pfrank+1))/np.float64(nda):
            count = count + 1
    
    particles = np.zeros(count, dtype='i')
    rtmp = -1
    for particle in range(nens):
        if np.float64(particle) >= np.float64(nens*pfrank)/np.float64(nda) and np.float64(particle) < np.float64(nens*(pfrank+1))/np.float64(nda):
            rtmp = rtmp + 1
            particles[rtmp] = particle
    
    print 'PFrank = '+str(pfrank)+' and I own particles '+str(particles)
    return count,particles,cpl_mpi_comm,pf_mpi_comm,nens,nda,pfrank,world_id

##==========================================================================
##The following two numbers can be changed to match the number of iterations
##and the size of the state vector from the model. 
num_iters = 40
state_dim = 20
##==========================================================================

state_vector = np.zeros(state_dim, dtype='float64')

count,particles,cpl_mpi_comm,pf_mpi_comm,nens,nda,pfrank,mype_id = initialise_mpi() #call subroutine to initialise EMPIRE

#This is the initial receive from the model                                        
for i in range(count):
    particle = particles[i]
    cpl_mpi_comm.Recv([state_vector,state_dim,MPI.DOUBLE],source=particle,tag=1)

print 'DA '+str(pfrank)+' has received all initial state_vectors over mpi'

#Create an initial ensemble

#start the timestepping loop
for iter in range(num_iters):
    for i in range(count):
        particle = particles[i]
        tag = 1
        cpl_mpi_comm.Send([state_vector,state_dim,MPI.DOUBLE],dest=particle,tag=1)
        print 'Particle filter '+str(pfrank)+' has sent state_vector over mpi at iteration '+str(iter)+' to ensemble member '+str(particle)
        
    for i in range(count):
        particle = particles[i]
        tag = 1
        cpl_mpi_comm.Recv([state_vector,state_dim,MPI.DOUBLE],source=particle,tag=1)
    print 'all models received by particle filter at iteration '+str(iter)
    
#now do the final send to the model
tag = 1
for i in range(count):
    particle = particles[i]
    tag = 1
    cpl_mpi_comm.Send([state_vector,state_dim,MPI.DOUBLE],dest=particle,tag=1)
    print 'Particle filter '+str(pfrank)+' has sent final state_vector over mpi to ensemble member '+str(particle)
