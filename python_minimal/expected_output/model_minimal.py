#! /usr/bin/env python
# Time-stamp: <2014-11-12 10:31:56 pbrowne>
# Python by Philip Browne

###############################################################################
#    model_minimal.py skeleton code showing how EMPIRE coupling is implemented
#
#The MIT License (MIT)
#
#Copyright (c) 2014 Philip A. Browne
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
#
#Email: p.browne@reading.ac.uk
###############################################################################

from mpi4py import MPI #import MPI stuff
import sys
import numpy as np

def initialise_mpi():
    #call mpi_init ( mpi_err )
    #not needed as called automatically when importing mpi4py
    global cpl_root,mdl_mpi_comm,cpl_mpi_comm,cpl_id,world_id,mdl_id,ptcl_id


    mdl_num_proc = 4
    da = np.array(0, dtype='i')
    mpi_comm_world = MPI.COMM_WORLD
    world_id = mpi_comm_world.Get_rank()
    world_size = mpi_comm_world.Get_size()

    models=mpi_comm_world.Split(da,world_id)
    models_size= models.Get_size()
    models_id  = models.Get_rank()
    mdlcolour = models_id / mdl_num_proc
    mdl_mpi_comm = models.Split(mdlcolour,models_id)
    mdl_id = mdl_mpi_comm.Get_rank()
    if ( mdl_id == 0):
        couple_colour = 9999
    else:
        couple_colour = MPI.UNDEFINED
    cpl_mpi_comm =  mpi_comm_world.Split(couple_colour,mdlcolour)
    if ( mdl_id == 0):
        nens = cpl_mpi_comm.Get_size()
        ptcl_id = cpl_mpi_comm.Get_rank()
        nda = world_size-models_size
        nens = nens-nda
        for da in range(1,nda+1):
            if ( ptcl_id < np.float64((nens*(da)))/np.float64(nda)):
                cpl_root = da-1 + nens
                break
            else:
                cpl_root = -1


initialise_mpi() #call subroutine to initialise EMPIRE

state_dim = 20
num_iters = 40
state_vector = np.ones(state_dim, dtype='float64')
tag=1

print 'MINIMAL MODEL: Global id = '+str(world_id)
print 'MINIMAL MODEL: Model id = '+str(mdl_id)+' with model communicator '+str(mdl_mpi_comm)
if(mdl_id == 0):
   print 'MINIMAL MODEL: as model id = 0 we have particle_id = ',ptcl_id
   print 'MINIMAL MODEL: the cpl_root is '+str(cpl_root)


if(mdl_id == 0):
    cpl_mpi_comm.Send([state_vector,state_dim,MPI.DOUBLE_PRECISION],dest=cpl_root,  tag=1)
    cpl_mpi_comm.Recv([state_vector,state_dim,MPI.DOUBLE_PRECISION],source=cpl_root,tag=1,status=None) 

for iteration in range(num_iters):
    state_vector = 1.5*state_vector
    if(mdl_id == 0):
        cpl_mpi_comm.Send([state_vector,state_dim,MPI.DOUBLE_PRECISION],dest=cpl_root,  tag=1)
        cpl_mpi_comm.Recv([state_vector,state_dim,MPI.DOUBLE_PRECISION],source=cpl_root,tag=1,status=None) 
        
    if(mdl_id == 0):
        print str(iteration)+' '+str(state_vector[1])
            


