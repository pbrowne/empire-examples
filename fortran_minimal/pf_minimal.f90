program pf_minimal
  implicit none
  include 'mpif.h'

  integer :: mpi_err,tag,state_dim,iter
  integer :: couple_colour,num_iters
  integer :: particle
  integer :: rtmp,cpl_mpi_comm,myrank
  integer :: nens,pfrank
  integer :: count,i,pf_colour,pf_mpi_comm
  integer,allocatable, dimension(:) :: particles

  integer :: da, nda, world_size,world_id
  


  real(kind=kind(1.0D0)),allocatable,dimension(:) :: state_vector

  integer :: mpi_status(mpi_status_size)


!!==========================================================================
!!The following two numbers can be changed to match the number of iterations
!!and the size of the state vector from the model.
  num_iters = 40
  state_dim = 20
!!==========================================================================

  allocate(state_vector(state_dim),stat=mpi_err)
  if(mpi_err .ne. 0) write(*,*) 'Particle filter could not allocate st&
       &ate_vector'

  pf_colour = 10000
  couple_colour=9999
  call MPI_INIT (mpi_err)

  da = 1
  call MPI_COMM_RANK (MPI_COMM_WORLD,world_id,     mpi_err)
  call mpi_comm_size (mpi_comm_world,world_size,   mpi_err)
  call mpi_comm_split(mpi_comm_world,da,           world_id,  pf_mpi_comm, mpi_err)
  call mpi_comm_rank (pf_mpi_comm,   pfrank,       mpi_err)
  call mpi_comm_size (pf_mpi_comm,   nda,          mpi_err)
  call MPI_COMM_SPLIT(MPI_COMM_WORLD,couple_colour,world_size,CPL_MPI_COMM,mpi_err)
  call MPI_COMM_RANK (CPL_MPI_COMM,  myRank,       mpi_err)
  call MPI_COMM_SIZE (CPL_MPI_COMM,  nens,         mpi_err)

  nens = nens-nda
  print*,'DA'
  print*,'nens = ',nens
  print*,'nda = ',nda


  !lets find the particles:
  count = 0
  do particle = 1,nens
     if( real(particle-1) .ge. real(nens*(pfrank))/real(nda) .and. real(particle-1) .lt. real(nens*(pfrank+1))/real(nda)) then
        count = count + 1
     end if
  end do

  allocate(particles(count))
  rtmp = 0
  do particle = 1,nens
     if(real(particle-1) .ge. real(nens*(pfrank))/real(nda) .and.&
          & real(particle-1) .lt. real(nens*(pfrank+1))/real(nda))&
          & then
        rtmp = rtmp + 1
        particles(rtmp) = particle
     end if
  end do


  
  print*,'PFrank = ',pfrank,' and I own particles ',particles
  
 
  !This is the initial receive from the model
  tag = 1        
  do i = 1,count
     particle = particles(i)
     call mpi_recv(state_vector, state_dim, MPI_DOUBLE_PRECISION, &
          particle-1, tag, cpl_mpi_comm,mpi_status, mpi_err)
  end do

  print*,'Particle filter ',pfrank,'has received all initial state_vectors over mpi'
  !Create an intitial ensemble

  
  !Start the timestepping loop
  do iter = 1,num_iters
     do i = 1,count
        particle = particles(i)
        tag = 1
        call mpi_send(state_vector, state_dim , MPI_DOUBLE_PRECISION, &
             particle-1, tag, cpl_mpi_comm, mpi_err)
        print*,'Particle filter ',pfrank,'has sent state_vector over mpi at iteratio&
             &n',iter,' to ensemble member ',particle
     end do

     
     do i = 1,count
        particle = particles(i)
        tag = 1
        call mpi_recv(state_vector, state_dim, MPI_DOUBLE_PRECISION, &
             particle-1, tag, cpl_mpi_comm,&
             mpi_status, mpi_err)
     end do
     
     print*,'all models received by particle filter at iteration ',iter


  end do
  
  !Now do the final send to the model
  tag = 1        
  do i = 1,count
     particle = particles(i)
     call mpi_send(state_vector, state_dim , MPI_DOUBLE_PRECISION, &
          particle-1, tag, cpl_mpi_comm, mpi_err)
     print*,'Particle filter ',pfrank,'has sent final state_vector over mpi &
          &to ensemble member ',particle
  end do
  !cleanup
  call MPI_FINALIZE(mpi_err)
  deallocate(state_vector)
end program pf_minimal
