#!/bin/bash
set -o nounset
set -o errexit


mkdir -p expected_output
cp minimal_model minimal_pf run.sh expected_output/
cd expected_output/
rm out*

run.sh


