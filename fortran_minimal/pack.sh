#!/bin/bash
set -o nounset
set -o errexit

make clean
make
test.sh

tarname='fortran_empire.tar'

rm -f $tarname

tar -cvf $tarname Makefile pf_minimal.f90 model_minimal.f90 expected_output/out*




